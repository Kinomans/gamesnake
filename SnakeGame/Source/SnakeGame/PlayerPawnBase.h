// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass ;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "SnakeBase")
	void CreateSnakeActor();

	UFUNCTION(BlueprintCallable, Category = "SnakeBase")
		int32 GetScore();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	UFUNCTION()
		void HandlePlayerPauseInput(float ButtonVal);
	
	//�������� �� Y � X ���� �������� �����
	float MaxY = 1030.f;			float MaxX = 500.f;
	float MinY = -1030.f;			float MinX = -530.f;	
	//�������� �� ��� Z  �������� ������
	float SpawnZ = 0.f;
	//���������� ������� �������� ��� � ��������� ������
	void AddRandomApple();
	float StepDeley = 2.0f;
	float BuferTime = 7.0f;

	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable , Category = "SnakeBase")

		int32 GetGameMode() const { return GameMode;}
};
