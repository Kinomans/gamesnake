// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDethActor.h"

// Sets default values
AMyDethActor::AMyDethActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MyRootComponent;


	class UStaticMeshComponent* WallChank;
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	WallChank->AttachTo(MyRootComponent);
		
}

// Called when the game starts or when spawned
void AMyDethActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDethActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();
}

void AMyDethActor::CollideWall()
{
	TArray<AActor*> CollectedActors;
	GetOverlappingActors(CollectedActors);
	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		CollectedActors[i]->Destroy(true, true);
	}

}

